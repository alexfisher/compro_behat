# Compro behat

Compro behat is blank behat project built on the [Behat Drupal Extension](https://github.com/jhedstrom/drupalextension).

## Requirements

- PHP **5.3.5+** with curl, mbstring, xml
- Composer
- Java
- cURL
- [Selenium](http://docs.seleniumhq.org/download/)

## Installation

    composer install

## Configuration

Configuration can be found in the behat.yml file. The base_url is set to the default
Vagrant URL for compro work. There is a blank drupal.feature file in the features
directory as a starter for adding feature tests.